package com.example.cinema.web;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.cinema.doa.FilmRepository;
import com.example.cinema.doa.TicketRepository;
import com.example.cinema.entities.Film;
import com.example.cinema.entities.Ticket;

import lombok.Data;

@RestController
public class CinemaRestController {
	@Autowired
	private FilmRepository filmRepository;
	@Autowired
	private TicketRepository ticketRepository;
	@GetMapping(path="/imageFilm/{id}",produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] image(@PathVariable(name = "id")Long id) throws Exception {
		Film film=filmRepository.findById(id).get();
		String photo=film.getPhoto();
		File file=new File(System.getProperty("user.home")+"/cinema/image/"+photo);
		Path path=Paths.get(file.toURI());
		return Files.readAllBytes(path);
	}
	
	@PostMapping("/payerTicket")
	public List<Ticket> payerTickets(@RequestBody  TicketForm ticketForm){
		
		List<Ticket> listTikcet=new ArrayList<>();
		ticketForm.getTickets().forEach(idTicket->{
Ticket ticket = ticketRepository.findById(idTicket).get();
ticket.setNomClient(ticketForm.getNomClient());
ticket.setReservee(true);
ticketRepository.save(ticket);
listTikcet.add(ticket);
});			
		
	return listTikcet;	
		
	}
}
@Data
class TicketForm {
private String nomClient;
private List<Long> tickets= new ArrayList<>();}




	
	
	