package com.example.cinema.serviceImp;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.cinema.doa.CategorieRepository;
import com.example.cinema.doa.CinemaRepository;
import com.example.cinema.doa.FilmRepository;
import com.example.cinema.doa.PlaceRepository;
import com.example.cinema.doa.ProjectionFilmRepository;
import com.example.cinema.doa.SalleRepository;
import com.example.cinema.doa.SeanceRepository;
import com.example.cinema.doa.TicketRepository;
import com.example.cinema.doa.VilleRepository;
import com.example.cinema.entities.Categorie;
import com.example.cinema.entities.Cinema;
import com.example.cinema.entities.Film;
import com.example.cinema.entities.Place;
import com.example.cinema.entities.ProjectionFilm;
import com.example.cinema.entities.Salle;
import com.example.cinema.entities.Seance;
import com.example.cinema.entities.Ticket;
import com.example.cinema.entities.Ville;
import com.example.cinema.service.ICinemaInitService;


@Service
@Transactional
public class CinemaInitServiceImpl implements ICinemaInitService {

	@Autowired
	private VilleRepository villeRepository;
	@Autowired
	private CinemaRepository cinemaRepository;
	@Autowired
	private SalleRepository salleRepository;
	@Autowired
	private ProjectionFilmRepository projectionFilmRepository;
	@Autowired
	private SeanceRepository seanceRepository;
	@Autowired
	private TicketRepository ticketRepository;
	@Autowired
	private CategorieRepository categorieRepository;
	@Autowired
	private FilmRepository filmRepository;
	@Autowired
	private PlaceRepository placeRepository;
	
	
	@Override
	public void initVilles() {
		Stream.of("Casablanca","Marrakech","Ouarzazate","Agadir").forEach(nameVille->{
			Ville ville=new Ville();
			ville.setNome(nameVille);
			villeRepository.save(ville);
		});
	}

	@Override
	public void initCinemas() {

		villeRepository.findAll().forEach(ville->{
			Stream.of("MegaRama","FOUNOUN","CHAHRAZAD","ATLASS")
			.forEach(nameCinema->{
				Cinema cinema =new Cinema();
				cinema.setName(nameCinema);
				//on suppose que nomber de salle est (min 3 max 7))
				cinema.setNomberSalles(3+(int)(Math.random()*7));
				cinema.setVille(ville);
				
				cinemaRepository.save(cinema);
						
			});
			
		});
	}

	@Override
	public void initSalles() {
		cinemaRepository.findAll().forEach(cinema->{
			for (int i = 0; i <cinema.getNomberSalles(); i++) {
				Salle salle = new Salle();
				salle.setCinema(cinema);
				salle.setNome("Salle"+(i+1));
				//on suppose que nomber de Place est 50 (min 20 max 50)
				salle.setNomberPlaces(20+(int)(Math.random()*30));
				salleRepository.save(salle);
			}
		});
		
	}

	@Override
	public void initPlaces() {
		salleRepository.findAll().forEach(salle->{
			for (int i = 0; i <salle.getNomberPlaces(); i++) {
				Place place= new Place();
				place.setNumero(i+1);
				place.setSalle(salle);
				placeRepository.save(place);
			}
			
		});
	}

	@Override
	public void initSeances() {
		DateFormat df = new SimpleDateFormat("HH:mm");
		 Stream.of("12:00","15:00","17:00","19:00","20:00","21:00").forEach(sean->{
			 Seance seance= new Seance();
			 try {
				seance.setHeureDebut(df.parse(sean));
				seanceRepository.save(seance);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 });
	}

	@Override
	public void initCategories() {		
		Stream.of("Action","Drama","Fliction","Histoir","Sport").forEach(cat->{
			
			Categorie categorie= new Categorie();
			categorie.setName(cat);
			categorieRepository.save(categorie);
		});
	}

	@Override
	public void initFilms() {
		//on definie duree du filme aleatoire 
		double []duree = new double[] {1,1.5,2,2.5};
		
		List<Categorie> categories= categorieRepository.findAll();
		Stream.of("Camille","Dora","I Kill Gaints","joker","vikings").forEach(titrefilm->{
			
			Film film = new Film();
			film.setTitre(titrefilm);
			film.setDuree(duree[new Random().nextInt(duree.length)]);
			film.setPhoto(titrefilm.replaceAll(" ", "")+".jpg");
			//categorie aleatoir
			film.setCategorie(categories.get(new Random().nextInt(categories.size())));
			filmRepository.save(film); 
		});
	}

	@Override
	public void initProjectionFilms() {
		double []prix = new double[] {30,90,70,60,50};
		villeRepository.findAll().forEach(ville->{
			ville.getCinema().forEach(cinema->{
				cinema.getSalles().forEach(salle->{
					filmRepository.findAll().forEach(film->{
						seanceRepository.findAll().forEach(seance->{
							
							ProjectionFilm projection =new ProjectionFilm();
							projection.setDateProjection(new Date());
							projection.setFilm(film);
							projection.setSeance(seance);
							projection.setSalle(salle);
							projection.setPrix(prix[new Random().nextInt(prix.length)]);
							projectionFilmRepository.save(projection);
						});
					});
				});
			});
		});
		
		
	}

	@Override
	public void initTickets() {
projectionFilmRepository.findAll().forEach(projection->{
	projection.getSalle().getPlaces().forEach(place->{
		
		Ticket ticket = new Ticket();
		ticket.setPlace(place);
		ticket.setProjectionFilm(projection);
		ticket.setPrix(projection.getPrix());
		ticket.setReservee(false);
		ticketRepository.save(ticket);
	});
		
		

	
});		
	}

}
