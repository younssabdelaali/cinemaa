package com.example.cinema.doa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.cinema.entities.Film;

@RepositoryRestResource
public interface FilmRepository extends JpaRepository<Film, Long> {

}
