package com.example.cinema.doa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.cinema.entities.Cinema;


@RepositoryRestResource
public interface CinemaRepository extends JpaRepository<Cinema, Long> {

}
