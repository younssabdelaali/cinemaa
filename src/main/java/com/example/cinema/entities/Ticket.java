package com.example.cinema.entities;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.cache.spi.access.AccessType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor 
@ToString
public class Ticket implements Serializable  {

	
	@Id @GeneratedValue( strategy = GenerationType.IDENTITY )
	private Long id ;
	private String nomClient ;
	private double prix;
	@Column(unique=true,nullable=true)
	private Integer codePayement ;
	private boolean reservee;

	
	@ManyToOne
	private Place place ;
	@ManyToOne
	@JsonIgnore
	private ProjectionFilm projectionFilm;

}
