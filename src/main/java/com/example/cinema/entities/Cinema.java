package com.example.cinema.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor 
@ToString
public class Cinema implements Serializable{

	@Id@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name ;
	private double longitude ;
	private double latitude;
	private double altitude ;
	private int nomberSalles;

	@ManyToOne
	private Ville ville ;

	@OneToMany(mappedBy = "cinema",fetch = FetchType.LAZY)
	private Collection<Salle> salles;
	
	
	
	
}
