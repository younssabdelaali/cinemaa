package com.example.cinema.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor 
@ToString
public class Film implements Serializable {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id ;
	private String titre;
	private double duree;
	private String realisateur ;
	private String description ;
	private String photo ;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateSortie ;
	
	@OneToMany(mappedBy = "film")
	private Collection<ProjectionFilm> projectionFilms ;


	@ManyToOne
	private Categorie categorie;



}
